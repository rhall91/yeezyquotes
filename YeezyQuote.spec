# -*- mode: python -*-

block_cipher = None


a = Analysis(['YeezyQuote.py'],
             pathex=['C:\\Users\\rhall\\Desktop\\pyscripts\\yeezy'],
             binaries=[],
             datas=[('yeezy_1000.dat', '.'), ('icon.dat', '.'), ('kanyes_Best.wav', '.')],
             hiddenimports=['requests'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='YeezyQuote',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='kanye.ico')
