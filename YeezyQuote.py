import tkinter as tk
import requests
import sys
import os
import winsound


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


app = tk.Tk()

HEIGHT = 500
WIDTH = 700

app.wm_title("Yeezy Quote")
app.iconbitmap(resource_path('kanye.ico'))
app.resizable(width=False, height=False)


def format_response(json):
    try:
        quote = json['quote']
    except:
        quote = 'There was a problem retrieving that information'

    return quote


def get_kanye_quote():
    url = 'https://api.kanye.rest/'
    headers = {"content-type": "application/json", 'user-agent': 'just-learning-programming'}
    response = requests.get(url, headers)

    results['text'] = format_response(response.json())

    winsound.PlaySound(resource_path('kanyes_best.wav'), winsound.SND_ASYNC)


C = tk.Canvas(app, height=HEIGHT, width=WIDTH)
background_image = tk.PhotoImage(file=resource_path('yeezy_1000.png'))
background_label = tk.Label(app, image=background_image)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

C.pack()

frame = tk.Frame(app, bg='#42c2f4', bd=10)
frame.place(relx=0.5, rely=0.15, relwidth=0.50, relheight=0.6, anchor='n')

bg_color = 'white'
results = tk.Label(frame, anchor='nw', justify='left', bd=4, wraplength=325)
results.config(font=40, bg=bg_color)
results.place(relwidth=1, relheight=0.75)


submit = tk.Button(frame, text='Give \'Em Kanye\'s Best', font=40, command=lambda: get_kanye_quote())
submit.place(relx=0.15, rely=0.8, relwidth=0.75, relheight=0.15)

app.mainloop()
